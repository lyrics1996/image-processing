﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace Demo
{
    public class MainViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private WriteableBitmap _previewImage1;

        public WriteableBitmap PreviewImage1
        {
            get { return _previewImage1; }
            set { _previewImage1 = value; PropertyChanged?.Invoke(this,new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage2;

        public WriteableBitmap PreviewImage2
        {
            get { return _previewImage2; }
            set { _previewImage2 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage3;

        public WriteableBitmap PreviewImage3
        {
            get { return _previewImage3; }
            set { _previewImage3 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage4;

        public WriteableBitmap PreviewImage4
        {
            get { return _previewImage4; }
            set { _previewImage4 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }


        private WriteableBitmap _previewImage5;

        public WriteableBitmap PreviewImage5
        {
            get { return _previewImage5; }
            set { _previewImage5 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage6;

        public WriteableBitmap PreviewImage6
        {
            get { return _previewImage6; }
            set { _previewImage6 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        public MainViewModel() {
            ImageProcess();
        }

        private BitmapImage Bitmap2BitmapImage(Bitmap bitmap ) {
            using (MemoryStream ms = new MemoryStream()) {
                bitmap.Save(ms,ImageFormat.Bmp);
                BitmapImage bi = new BitmapImage();
                bi.BeginInit();
                bi.StreamSource = ms;
                bi.CacheOption = BitmapCacheOption.OnLoad;
                bi.EndInit();
                bi.Freeze();
                return bi;
            }
        }
        private void ImageProcess()
        {
            // 1. 加载原图1
            var image1 = new Image<Bgr, byte>("bird1.png");
            PreviewImage1 = new WriteableBitmap(Bitmap2BitmapImage(image1.Bitmap));

            // 2. 加载原图2
            var image2 = new Image<Bgr, byte>("bird2.png");
            PreviewImage2 = new WriteableBitmap(Bitmap2BitmapImage(image2.Bitmap));

            // 3. 图像相与
            var image3 = new Image<Bgr, byte>(image1.Size);
            CvInvoke.BitwiseAnd(image1, image2, image3);
            PreviewImage3 = new WriteableBitmap(Bitmap2BitmapImage(image3.Bitmap));

            // 4. 图像相或
            var image4 = new Image<Bgr, byte>(image1.Size);
            CvInvoke.BitwiseOr(image1, image2, image4);
            PreviewImage4 = new WriteableBitmap(Bitmap2BitmapImage(image4.Bitmap));

            // 5. 图像取反
            var image5 = new Image<Bgr, byte>(image1.Size);
            CvInvoke.BitwiseNot(image1, image5);
            PreviewImage5 = new WriteableBitmap(Bitmap2BitmapImage(image5.Bitmap));

            // 6. 图像亦或
            var image6 = new Image<Bgr, byte>(image1.Size);
            CvInvoke.BitwiseXor(image1, image2, image6);
            PreviewImage6 = new WriteableBitmap(Bitmap2BitmapImage(image6.Bitmap));

            //// 3. 图像的加法
            //var image3 = new Image<Bgr, byte>(image1.Size);
            //CvInvoke.Add(image1, image2, image3);
            //// CvInvoke.AddWeighted(image1, 0.5, image2, 0.5, 0, image3); // 按权重加
            //PreviewImage3 = new WriteableBitmap(Bitmap2BitmapImage(image3.Bitmap));

            //// 4. 图像的减法
            //var image4 = new Image<Bgr, byte>(image1.Size);
            //CvInvoke.Subtract(image1, image2, image4);
            //// CvInvoke.AbsDiff(image1, image2, image4); // 相减后取绝对值
            //PreviewImage4 = new WriteableBitmap(Bitmap2BitmapImage(image4.Bitmap));

            //// 5. 图像的乘法
            //var image5 = new Image<Bgr, byte>(image1.Size);
            //CvInvoke.Multiply(image1, image2, image5);
            //PreviewImage5 = new WriteableBitmap(Bitmap2BitmapImage(image5.Bitmap));

            //// 6. 图像的除法
            //var image6 = new Image<Bgr, byte>(image1.Size);
            //CvInvoke.Divide(image1, image2, image6);
            //PreviewImage6 = new WriteableBitmap(Bitmap2BitmapImage(image6.Bitmap));

        }
    }
}
