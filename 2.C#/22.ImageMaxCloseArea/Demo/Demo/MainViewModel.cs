﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Threading;
using System.Windows.Media.Imaging;
using Tesseract;

namespace Demo
{
    public class MainViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private WriteableBitmap _previewImage1;

        public WriteableBitmap PreviewImage1
        {
            get { return _previewImage1; }
            set { _previewImage1 = value; PropertyChanged?.Invoke(this,new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage2;

        public WriteableBitmap PreviewImage2
        {
            get { return _previewImage2; }
            set { _previewImage2 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage3;

        public WriteableBitmap PreviewImage3
        {
            get { return _previewImage3; }
            set { _previewImage3 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage4;

        public WriteableBitmap PreviewImage4
        {
            get { return _previewImage4; }
            set { _previewImage4 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }


        private WriteableBitmap _previewImage5;

        public WriteableBitmap PreviewImage5
        {
            get { return _previewImage5; }
            set { _previewImage5 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage6;

        public WriteableBitmap PreviewImage6
        {
            get { return _previewImage6; }
            set { _previewImage6 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage7;

        public WriteableBitmap PreviewImage7
        {
            get { return _previewImage7; }
            set { _previewImage7 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }
        private WriteableBitmap _previewImage8;

        public WriteableBitmap PreviewImage8
        {
            get { return _previewImage8; }
            set { _previewImage8 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }
        private WriteableBitmap _previewImage9;

        public WriteableBitmap PreviewImage9
        {
            get { return _previewImage9; }
            set { _previewImage9 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage10;

        public WriteableBitmap PreviewImage10
        {
            get { return _previewImage10; }
            set { _previewImage10 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }
        private WriteableBitmap _previewImage11;

        public WriteableBitmap PreviewImage11
        {
            get { return _previewImage11; }
            set { _previewImage11 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }
        private WriteableBitmap _previewImage12;

        public WriteableBitmap PreviewImage12
        {
            get { return _previewImage12; }
            set { _previewImage12 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }
        private WriteableBitmap _previewImage13;

        public WriteableBitmap PreviewImage13
        {
            get { return _previewImage13; }
            set { _previewImage13 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }
        private WriteableBitmap _previewImage14;

        public WriteableBitmap PreviewImage14
        {
            get { return _previewImage14; }
            set { _previewImage14 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }
        private WriteableBitmap _previewImage15;

        public WriteableBitmap PreviewImage15
        {
            get { return _previewImage15; }
            set { _previewImage15 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }
        private WriteableBitmap _previewImage16;

        public WriteableBitmap PreviewImage16
        {
            get { return _previewImage16; }
            set { _previewImage16 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }
        public MainViewModel() {
            ImageProcess();
        }

        private BitmapImage Bitmap2BitmapImage(Bitmap bitmap ) {
            using (MemoryStream ms = new MemoryStream()) {
                bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                BitmapImage bi = new BitmapImage();
                bi.BeginInit();
                bi.StreamSource = ms;
                bi.CacheOption = BitmapCacheOption.OnLoad;
                bi.EndInit();
                bi.Freeze();
                return bi;
            }
        }
        private Bitmap Text(Bitmap img,string str) {
            Bitmap bmp = new Bitmap(img.Width, img.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            using (Graphics g = Graphics.FromImage(bmp))
            {
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                g.DrawImage(img, 0, 0, bmp.Width, bmp.Height);
                g.DrawString(str, new Font("Verdana", 50), new SolidBrush(Color.Black), new PointF(0, 0));
            }
            return bmp;

        }
        private Bitmap Text2(Bitmap img, string str)
        {
            Bitmap bmp = new Bitmap(img.Width, img.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            using (Graphics g = Graphics.FromImage(bmp))
            {
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                g.DrawImage(img, 0, 0, bmp.Width, bmp.Height);
                g.DrawString(str, new Font("Verdana", 50), new SolidBrush(Color.White), new PointF(0, 0));
            }
            return bmp;

        }

        private Bitmap Text3(Bitmap img, string str)
        {
            Bitmap bmp = new Bitmap(img.Width, img.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            using (Graphics g = Graphics.FromImage(bmp))
            {
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                g.DrawImage(img, 0, 0, bmp.Width, bmp.Height);
                g.DrawString(str, new Font("Verdana", 10), new SolidBrush(Color.Black), new PointF(0, 0));
            }
            return bmp;

        }
        private Mat GetMaxCloseArea(string imgPath) {
            // 1. 读取原图
            var img = new Mat(imgPath);

            // 2. 灰度
            var imgGray = new Mat(imgPath, 0);

            // 3. 阈值
            var img3 = new Mat();
            CvInvoke.Threshold(imgGray, img3, 0, 255, ThresholdType.Otsu | ThresholdType.BinaryInv);

            // 4. 查找轮廓
            VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint();
            Mat hierarchy = new Mat();
            CvInvoke.FindContours(img3, contours, hierarchy, RetrType.Tree, ChainApproxMethod.ChainApproxNone);
            if (contours.Size == 1) {
                return img;
            }
            // 5. 排序
            var array = contours.ToArrayOfArray();
            ;
            for (int i = 0; i < contours.Size - 1; i++)
            {
                for (int j = 0; j < contours.Size - i - 1; j++)

                    if (CvInvoke.ContourArea(new VectorOfPoint(array[j])) < CvInvoke.ContourArea(new VectorOfPoint(array[j + 1])))
                    {
                        var temp = array[j + 1];
                        array[j + 1] = array[j];
                        array[j] = temp;
                    }
            }
          

            VectorOfVectorOfPoint contoursSorted = new VectorOfVectorOfPoint(array);
            var e = CvInvoke.ArcLength(contoursSorted[1], true) * 0.01;
            VectorOfPoint s = new VectorOfPoint();
            CvInvoke.ApproxPolyDP(contoursSorted[1], s, e, true);
            VectorOfVectorOfPoint contours3 = new VectorOfVectorOfPoint();
            contours3.Push(s);
            var img4 = img.Clone();
            CvInvoke.DrawContours(img4, contours3, 0, new MCvScalar(0, 0, 255), 2);
            return img4;
        
        }
        private void ImageProcess()
        {
            Mat image1 = new Mat("Test1.png");
            PreviewImage1 = new WriteableBitmap(Bitmap2BitmapImage(Text(image1.Bitmap, "原图1")));
            PreviewImage2 = new WriteableBitmap(Bitmap2BitmapImage(Text(GetMaxCloseArea("Test1.png").Bitmap, "最大闭合区域")));

            Mat image2 = new Mat("Test7.png");
            PreviewImage3 = new WriteableBitmap(Bitmap2BitmapImage(Text(image2.Bitmap, "原图2")));
            PreviewImage4 = new WriteableBitmap(Bitmap2BitmapImage(Text(GetMaxCloseArea("Test7.png").Bitmap, "最大闭合区域")));

            Mat image3 = new Mat("Test3.png");
            PreviewImage5 = new WriteableBitmap(Bitmap2BitmapImage(Text(image3.Bitmap, "原图3")));
            PreviewImage6= new WriteableBitmap(Bitmap2BitmapImage(Text(GetMaxCloseArea("Test3.png").Bitmap, "最大闭合区域")));

            Mat image4 = new Mat("Test4.png");
            PreviewImage7 = new WriteableBitmap(Bitmap2BitmapImage(Text(image4.Bitmap, "原图4")));
            PreviewImage8 = new WriteableBitmap(Bitmap2BitmapImage(Text(GetMaxCloseArea("Test4.png").Bitmap, "最大闭合区域")));

            Mat image5 = new Mat("Test5.png");
            PreviewImage9 = new WriteableBitmap(Bitmap2BitmapImage(Text(image5.Bitmap, "原图5")));
            PreviewImage10 = new WriteableBitmap(Bitmap2BitmapImage(Text(GetMaxCloseArea("Test5.png").Bitmap, "最大闭合区域")));

            Mat image6 = new Mat("Test6.png");
            PreviewImage11 = new WriteableBitmap(Bitmap2BitmapImage(Text(image6.Bitmap, "原图6")));
            PreviewImage12 = new WriteableBitmap(Bitmap2BitmapImage(Text(GetMaxCloseArea("Test6.png").Bitmap, "最大闭合区域")));

            Mat image7 = new Mat("Test8.png");
            PreviewImage13 = new WriteableBitmap(Bitmap2BitmapImage(Text(image7.Bitmap, "原图7")));
            PreviewImage14 = new WriteableBitmap(Bitmap2BitmapImage(Text(GetMaxCloseArea("Test8.png").Bitmap, "最大闭合区域")));

            Mat image8 = new Mat("Test9.png");
            PreviewImage15 = new WriteableBitmap(Bitmap2BitmapImage(Text(image8.Bitmap, "原图8")));
            PreviewImage16 = new WriteableBitmap(Bitmap2BitmapImage(Text(GetMaxCloseArea("Test9.png").Bitmap, "最大闭合区域")));
        }
    }
}
