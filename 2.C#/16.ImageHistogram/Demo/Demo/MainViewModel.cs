﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Media.Imaging;

namespace Demo
{
    public class MainViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private WriteableBitmap _previewImage1;

        public WriteableBitmap PreviewImage1
        {
            get { return _previewImage1; }
            set { _previewImage1 = value; PropertyChanged?.Invoke(this,new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage2;

        public WriteableBitmap PreviewImage2
        {
            get { return _previewImage2; }
            set { _previewImage2 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage3;

        public WriteableBitmap PreviewImage3
        {
            get { return _previewImage3; }
            set { _previewImage3 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage4;

        public WriteableBitmap PreviewImage4
        {
            get { return _previewImage4; }
            set { _previewImage4 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }


        private WriteableBitmap _previewImage5;

        public WriteableBitmap PreviewImage5
        {
            get { return _previewImage5; }
            set { _previewImage5 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage6;

        public WriteableBitmap PreviewImage6
        {
            get { return _previewImage6; }
            set { _previewImage6 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage7;

        public WriteableBitmap PreviewImage7
        {
            get { return _previewImage7; }
            set { _previewImage7 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }
        private WriteableBitmap _previewImage8;

        public WriteableBitmap PreviewImage8
        {
            get { return _previewImage8; }
            set { _previewImage8 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }
        private WriteableBitmap _previewImage9;

        public WriteableBitmap PreviewImage9
        {
            get { return _previewImage9; }
            set { _previewImage9 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }
        public MainViewModel() {
            ImageProcess();
        }

        private BitmapImage Bitmap2BitmapImage(Bitmap bitmap ) {
            using (MemoryStream ms = new MemoryStream()) {
                bitmap.Save(ms,ImageFormat.Bmp);
                BitmapImage bi = new BitmapImage();
                bi.BeginInit();
                bi.StreamSource = ms;
                bi.CacheOption = BitmapCacheOption.OnLoad;
                bi.EndInit();
                bi.Freeze();
                return bi;
            }
        }
        private Bitmap Text(Bitmap img,string str) {
            Bitmap bmp = new Bitmap(img.Width, img.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            using (Graphics g = Graphics.FromImage(bmp))
            {
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                g.DrawImage(img, 0, 0, bmp.Width, bmp.Height);
                g.DrawString(str, new Font("Verdana", 100), new SolidBrush(Color.Black), new PointF(0, 0));
            }
            return bmp;

        }
        private Bitmap Text2(Bitmap img, string str)
        {
            Bitmap bmp = new Bitmap(img.Width, img.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            using (Graphics g = Graphics.FromImage(bmp))
            {
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                g.DrawImage(img, 0, 0, bmp.Width, bmp.Height);
                g.DrawString(str, new Font("Verdana", 50), new SolidBrush(Color.White), new PointF(0, 0));
            }
            return bmp;

        }

        private Bitmap Text3(Bitmap img, string str)
        {
            Bitmap bmp = new Bitmap(img.Width, img.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            using (Graphics g = Graphics.FromImage(bmp))
            {
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                g.DrawImage(img, 0, 0, bmp.Width, bmp.Height);
                g.DrawString(str, new Font("Verdana", 10), new SolidBrush(Color.Black), new PointF(0, 0));
            }
            return bmp;

        }
        private void ImageProcess()
        {
            //  1.加载原图
            var image1 = new Image<Bgr, byte>("bird1.png");
            var image0 = image1.Mat.Clone();
            PreviewImage1 = new WriteableBitmap(Bitmap2BitmapImage(Text(image1.Bitmap, "原图")));

            // 2. 原图转灰度
            var imgGray = new Mat();
            CvInvoke.CvtColor(image0, imgGray, ColorConversion.Bgr2Gray);
            PreviewImage2 = new WriteableBitmap(Bitmap2BitmapImage(Text(imgGray.Bitmap, "灰度")));

            // 3. 计算直方图
            var hist = new Mat();
            int[] channels = new int[] { 0 };  //初始化数组
            float[] ranges = new float[] { 0, 255 };
            int[] histSize = new int[] { 256 };
            VectorOfMat vMatImgs = new VectorOfMat();
            vMatImgs.Push(imgGray);
            CvInvoke.CalcHist(vMatImgs, channels, new Mat(),hist, histSize, ranges, false);
            var m = new Matrix<float>(256, 1);
            hist.CopyTo(m);
            // 3.1 简单的做个显示
            Point[] points = new Point[256];
            
            for (int i=0; i< m.Rows; i++) {
                
                points[i] = new Point(i*10, imgGray.Height * 3-(int)m[i, 0]);

            }
            var img = new Image<Gray,byte>(255*10, imgGray.Height*3,new Gray(255));
            CvInvoke.Polylines(img, points, true,new MCvScalar(0,0,0),10);
            PreviewImage3 = new WriteableBitmap(Bitmap2BitmapImage(Text(img.Bitmap, "直方图")));

            // 4. 直方图均衡化
            var img4 = new Mat();
            CvInvoke.EqualizeHist(imgGray, img4);
            PreviewImage4 = new WriteableBitmap(Bitmap2BitmapImage(Text(img4.Bitmap, "均衡")));

            // 5. 自适应均衡
            var img5= new Mat();
            CvInvoke.CLAHE(imgGray,2.0,new Size(8,8),img5);
            PreviewImage5 = new WriteableBitmap(Bitmap2BitmapImage(Text(img5.Bitmap, "自适应均衡")));

            // 6. 彩色图
            var img6 = image1.Split();
            var imgB = new Image<Gray, byte>(image1.Size);
            var imgG = new Image<Gray, byte>(image1.Size);
            var imgR = new Image<Gray, byte>(image1.Size);
            CvInvoke.EqualizeHist(img6[0], imgB);
            CvInvoke.EqualizeHist(img6[1], imgG);
            CvInvoke.EqualizeHist(img6[2], imgR);
            var imgC = new Image<Bgr, byte>(new Image<Gray, byte>[] { imgB, imgG, imgR }) ;
            PreviewImage6 = new WriteableBitmap(Bitmap2BitmapImage(Text(imgC.Bitmap, "均衡")));

            // 7. 彩色图自适应均衡
            CvInvoke.CLAHE(img6[0], 2.0, new Size(8, 8), imgB);
            CvInvoke.CLAHE(img6[1], 2.0, new Size(8, 8), imgG);
            CvInvoke.CLAHE(img6[2], 2.0, new Size(8, 8), imgR);
            var imgD = new Image<Bgr, byte>(new Image<Gray, byte>[] { imgB, imgG, imgR });
            PreviewImage7 = new WriteableBitmap(Bitmap2BitmapImage(Text(imgD.Bitmap, "自适应均衡")));

          

        }
    }
}
