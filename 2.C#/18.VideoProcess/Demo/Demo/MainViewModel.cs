﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Threading;
using System.Windows.Media.Imaging;

namespace Demo
{
    public class MainViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private WriteableBitmap _previewImage1;

        public WriteableBitmap PreviewImage1
        {
            get { return _previewImage1; }
            set { _previewImage1 = value; PropertyChanged?.Invoke(this,new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage2;

        public WriteableBitmap PreviewImage2
        {
            get { return _previewImage2; }
            set { _previewImage2 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage3;

        public WriteableBitmap PreviewImage3
        {
            get { return _previewImage3; }
            set { _previewImage3 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage4;

        public WriteableBitmap PreviewImage4
        {
            get { return _previewImage4; }
            set { _previewImage4 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }


        private WriteableBitmap _previewImage5;

        public WriteableBitmap PreviewImage5
        {
            get { return _previewImage5; }
            set { _previewImage5 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage6;

        public WriteableBitmap PreviewImage6
        {
            get { return _previewImage6; }
            set { _previewImage6 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage7;

        public WriteableBitmap PreviewImage7
        {
            get { return _previewImage7; }
            set { _previewImage7 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }
        private WriteableBitmap _previewImage8;

        public WriteableBitmap PreviewImage8
        {
            get { return _previewImage8; }
            set { _previewImage8 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }
        private WriteableBitmap _previewImage9;

        public WriteableBitmap PreviewImage9
        {
            get { return _previewImage9; }
            set { _previewImage9 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }
        public MainViewModel() {
            ImageProcess();
        }

        private BitmapImage Bitmap2BitmapImage(Bitmap bitmap ) {
            using (MemoryStream ms = new MemoryStream()) {
                bitmap.Save(ms,ImageFormat.Bmp);
                BitmapImage bi = new BitmapImage();
                bi.BeginInit();
                bi.StreamSource = ms;
                bi.CacheOption = BitmapCacheOption.OnLoad;
                bi.EndInit();
                bi.Freeze();
                return bi;
            }
        }
        private Bitmap Text(Bitmap img,string str) {
            Bitmap bmp = new Bitmap(img.Width, img.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            using (Graphics g = Graphics.FromImage(bmp))
            {
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                g.DrawImage(img, 0, 0, bmp.Width, bmp.Height);
                g.DrawString(str, new Font("Verdana", 100), new SolidBrush(Color.Black), new PointF(0, 0));
            }
            return bmp;

        }
        private Bitmap Text2(Bitmap img, string str)
        {
            Bitmap bmp = new Bitmap(img.Width, img.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            using (Graphics g = Graphics.FromImage(bmp))
            {
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                g.DrawImage(img, 0, 0, bmp.Width, bmp.Height);
                g.DrawString(str, new Font("Verdana", 50), new SolidBrush(Color.White), new PointF(0, 0));
            }
            return bmp;

        }

        private Bitmap Text3(Bitmap img, string str)
        {
            Bitmap bmp = new Bitmap(img.Width, img.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            using (Graphics g = Graphics.FromImage(bmp))
            {
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                g.DrawImage(img, 0, 0, bmp.Width, bmp.Height);
                g.DrawString(str, new Font("Verdana", 10), new SolidBrush(Color.Black), new PointF(0, 0));
            }
            return bmp;

        }
        private void ImageProcess()
        {
            //// 1. 打开摄像头
            //VideoCapture videoCapture = new VideoCapture(0);
            //while (true) {
            //    Mat frame = new Mat();
            //    videoCapture.Read(frame);
            //    CvInvoke.Imshow("video", frame);
            //    if (CvInvoke.WaitKey(30) == 27)
            //    {
            //        break;
            //    }
            //}

            // 2. 读取本地视频
            //VideoCapture videoCapture = new VideoCapture("01.mp4");
            //Thread.Sleep(100);
            //while (true)
            //{
            //    Mat frame = new Mat();
            //    videoCapture.Read(frame);
            //    CvInvoke.Imshow("video", frame);
            //    if (CvInvoke.WaitKey(30) == 27)
            //    {
            //        break;
            //    }
            //}

            // 3. 读取一张网络图片
            //VideoCapture videoCapture = new VideoCapture("https://img-pre.ivsky.com/img/tupian/pre/202109/09/xiniu.jpg");
            //Thread.Sleep(100);
            //while (true)
            //{
            //    try
            //    {
            //        Mat frame = new Mat();
            //        videoCapture.Read(frame);
            //        CvInvoke.Imshow("video", frame);
            //        if (CvInvoke.WaitKey(30) == 27)
            //        {
            //            break;
            //        }
            //    }
            //    catch (Exception ex) {
            //        break;
            //    }
                
            //}

            //// 4. 读取网络视频
            //VideoCapture videoCapture = new VideoCapture("http://flv0.bn.netease.com/04664904d20f94587232865acc5bed9acc0f3319aeb011e6c931f85707b577aba6ad8c7007f694915c92cb373fe6081bac7dbfb8f5c6fe548568efbda19afb8840eaa0570fde633091781fac3acc36d885e2146b17cd00ea0f5f3d3afbd9a84f557b24be0471d4c8825c53e32b510cca11032602353b0864.mp4");
            //Thread.Sleep(100);
            //while (true)
            //{
            //    try
            //    {
            //        Mat frame = new Mat();
            //        videoCapture.Read(frame);
            //        CvInvoke.Imshow("video", frame);
            //        if (CvInvoke.WaitKey(30) == 27)
            //        {
            //            break;
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        break;
            //    }

            //}

            //// 5. 读取本地序列图
            //VideoCapture videoCapture = new VideoCapture("test/%01d.jpg"); 
            //Thread.Sleep(100);
            //while (true)
            //{
            //    try
            //    {
            //        Mat frame = new Mat();
            //        videoCapture.Read(frame);
            //        CvInvoke.Imshow("video", frame);
            //        if (CvInvoke.WaitKey(30) == 27)
            //        {
            //            break;
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        break;
            //    }

            //}


            // 6. 保存网络视频
            VideoCapture videoCapture = new VideoCapture("http://flv0.bn.netease.com/04664904d20f94587232865acc5bed9acc0f3319aeb011e6c931f85707b577aba6ad8c7007f694915c92cb373fe6081bac7dbfb8f5c6fe548568efbda19afb8840eaa0570fde633091781fac3acc36d885e2146b17cd00ea0f5f3d3afbd9a84f557b24be0471d4c8825c53e32b510cca11032602353b0864.mp4");
            VideoWriter videoWriter = null;
            Thread.Sleep(100);
            while (true)
            {
                try
                {
                    Mat frame = new Mat();
                    videoCapture.Read(frame);
                    if (videoWriter == null) {
                        videoWriter = new VideoWriter("out.mp4", 0, 100, frame.Size, false);
                    }
                    videoWriter.Write(frame);
                    CvInvoke.Imshow("video", frame);
                    if (CvInvoke.WaitKey(30) == 27)
                    {
                        break;
                    }
                }
                catch (Exception ex)
                {
                    break;
                }

            }
        }
    }
}
