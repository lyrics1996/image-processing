﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Media.Imaging;

namespace Demo
{
    public class MainViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private WriteableBitmap _previewImage1;

        public WriteableBitmap PreviewImage1
        {
            get { return _previewImage1; }
            set { _previewImage1 = value; PropertyChanged?.Invoke(this,new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage2;

        public WriteableBitmap PreviewImage2
        {
            get { return _previewImage2; }
            set { _previewImage2 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage3;

        public WriteableBitmap PreviewImage3
        {
            get { return _previewImage3; }
            set { _previewImage3 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage4;

        public WriteableBitmap PreviewImage4
        {
            get { return _previewImage4; }
            set { _previewImage4 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }


        private WriteableBitmap _previewImage5;

        public WriteableBitmap PreviewImage5
        {
            get { return _previewImage5; }
            set { _previewImage5 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage6;

        public WriteableBitmap PreviewImage6
        {
            get { return _previewImage6; }
            set { _previewImage6 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage7;

        public WriteableBitmap PreviewImage7
        {
            get { return _previewImage7; }
            set { _previewImage7 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }
        private WriteableBitmap _previewImage8;

        public WriteableBitmap PreviewImage8
        {
            get { return _previewImage8; }
            set { _previewImage8 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }
        private WriteableBitmap _previewImage9;

        public WriteableBitmap PreviewImage9
        {
            get { return _previewImage9; }
            set { _previewImage9 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }
        public MainViewModel() {
            ImageProcess();
        }

        private BitmapImage Bitmap2BitmapImage(Bitmap bitmap ) {
            using (MemoryStream ms = new MemoryStream()) {
                bitmap.Save(ms,ImageFormat.Bmp);
                BitmapImage bi = new BitmapImage();
                bi.BeginInit();
                bi.StreamSource = ms;
                bi.CacheOption = BitmapCacheOption.OnLoad;
                bi.EndInit();
                bi.Freeze();
                return bi;
            }
        }
        private Bitmap Text(Bitmap img,string str) {
            Bitmap bmp = new Bitmap(img.Width, img.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            using (Graphics g = Graphics.FromImage(bmp))
            {
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                g.DrawImage(img, 0, 0, bmp.Width, bmp.Height);
                g.DrawString(str, new Font("Verdana", 20), new SolidBrush(Color.Black), new PointF(0, 0));
            }
            return bmp;

        }
        private Bitmap Text2(Bitmap img, string str)
        {
            Bitmap bmp = new Bitmap(img.Width, img.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            using (Graphics g = Graphics.FromImage(bmp))
            {
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                g.DrawImage(img, 0, 0, bmp.Width, bmp.Height);
                g.DrawString(str, new Font("Verdana", 20), new SolidBrush(Color.White), new PointF(0, 0));
            }
            return bmp;

        }
        private void ImageProcess()
        {
            // 1. 加载原图
            var image1 = new Image<Bgr, byte>("bird.png");
            //image1.Resize(160,160,Inter.Linear).Save("bird.png");
            var image0 = image1.Mat.Clone();
            PreviewImage1 = new WriteableBitmap(Bitmap2BitmapImage(Text(image1.Bitmap, "原图")));

            // 2.  向上操作
            Mat image2 = new Mat();
            CvInvoke.PyrUp(image0.Clone(),image2);
            PreviewImage2 = new WriteableBitmap(Bitmap2BitmapImage(Text(image2.Bitmap, "向上操作")));

            // 3.  向下操作
            Mat image3 = new Mat();
            CvInvoke.PyrDown(image0.Clone(), image3);
            PreviewImage3 = new WriteableBitmap(Bitmap2BitmapImage(Text(image3.Bitmap, "向下操作")));

            // 4. 高斯金字塔 1级
            PreviewImage4 = new WriteableBitmap(Bitmap2BitmapImage(Text(image0.Bitmap, "高斯 1级")));

            // 5. 高斯金字塔 2级
            Mat img5 = new Mat();
            CvInvoke.PyrDown(image0.Clone(), img5);
            PreviewImage5 = new WriteableBitmap(Bitmap2BitmapImage(Text(img5.Bitmap, "2级")));

            // 6. 高斯金字塔 3级
            Mat img6 = new Mat();
            CvInvoke.PyrDown(img5.Clone(), img6);
            PreviewImage6 = new WriteableBitmap(Bitmap2BitmapImage(Text(img6.Bitmap, "3")));

            // 7. 拉布拉斯 1 级
            var imgG = img6.Clone();
            var imgL = new Mat();
            var imgD = new Mat();
            var imgU = new Mat();
            CvInvoke.PyrDown(imgG,imgD);
            CvInvoke.PyrUp(imgD, imgU);
            imgL = imgG - imgU;
            PreviewImage7 = new WriteableBitmap(Bitmap2BitmapImage(Text2(imgL.Bitmap, "1")));

            // 8. 拉布拉斯 2 级
            imgG = img5.Clone();
            CvInvoke.PyrDown(imgG, imgD);
            CvInvoke.PyrUp(imgD, imgU);
            imgL = imgG - imgU;
            PreviewImage8 = new WriteableBitmap(Bitmap2BitmapImage(Text2(imgL.Bitmap, "2级")));

            // 9. 拉布拉斯 2 级
            imgG = image0.Clone();
            CvInvoke.PyrDown(imgG, imgD);
            CvInvoke.PyrUp(imgD, imgU);
            imgL = imgG - imgU;
            PreviewImage9 = new WriteableBitmap(Bitmap2BitmapImage(Text2(imgL.Bitmap, "拉布拉斯 3级")));

            //// 5. Canny
            //Mat image4 = new Mat();
            //CvInvoke.Canny(image3.Clone(),image4,50,150);
            //PreviewImage5 = new WriteableBitmap(Bitmap2BitmapImage(Text2(image4.Bitmap, "Canny 50,150")));

            //// 6. Canny
            //Mat image6 = new Mat();
            //CvInvoke.Canny(image3.Clone(), image6, 80, 100);
            //PreviewImage6 = new WriteableBitmap(Bitmap2BitmapImage(Text2(image6.Bitmap, "Canny 80,100")));

            //// 7. Canny
            //Mat image7 = new Mat();
            //CvInvoke.Canny(image3.Clone(), image7, 80, 150);
            //PreviewImage7 = new WriteableBitmap(Bitmap2BitmapImage(Text2(image7.Bitmap, "Canny 80,150")));

            //// 8. Canny
            //Mat image8 = new Mat();
            //CvInvoke.Canny(image3.Clone(), image8, 100, 150);
            //PreviewImage8 = new WriteableBitmap(Bitmap2BitmapImage(Text2(image8.Bitmap, "Canny 100,150")));

            //// 9. Canny
            //Mat image9 = new Mat();
            //CvInvoke.Canny(image3.Clone(), image9, 50, 80);
            //PreviewImage9 = new WriteableBitmap(Bitmap2BitmapImage(Text2(image9.Bitmap, "Canny 50,80")));
        }
    }
}
