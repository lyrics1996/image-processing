﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Media.Imaging;

namespace Demo
{
    public class MainViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private WriteableBitmap _previewImage1;

        public WriteableBitmap PreviewImage1
        {
            get { return _previewImage1; }
            set { _previewImage1 = value; PropertyChanged?.Invoke(this,new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage2;

        public WriteableBitmap PreviewImage2
        {
            get { return _previewImage2; }
            set { _previewImage2 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage3;

        public WriteableBitmap PreviewImage3
        {
            get { return _previewImage3; }
            set { _previewImage3 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage4;

        public WriteableBitmap PreviewImage4
        {
            get { return _previewImage4; }
            set { _previewImage4 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }


        private WriteableBitmap _previewImage5;

        public WriteableBitmap PreviewImage5
        {
            get { return _previewImage5; }
            set { _previewImage5 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage6;

        public WriteableBitmap PreviewImage6
        {
            get { return _previewImage6; }
            set { _previewImage6 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage7;

        public WriteableBitmap PreviewImage7
        {
            get { return _previewImage7; }
            set { _previewImage7 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }
        private WriteableBitmap _previewImage8;

        public WriteableBitmap PreviewImage8
        {
            get { return _previewImage8; }
            set { _previewImage8 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }
        private WriteableBitmap _previewImage9;

        public WriteableBitmap PreviewImage9
        {
            get { return _previewImage9; }
            set { _previewImage9 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }
        public MainViewModel() {
            ImageProcess();
        }

        private BitmapImage Bitmap2BitmapImage(Bitmap bitmap ) {
            using (MemoryStream ms = new MemoryStream()) {
                bitmap.Save(ms,ImageFormat.Bmp);
                BitmapImage bi = new BitmapImage();
                bi.BeginInit();
                bi.StreamSource = ms;
                bi.CacheOption = BitmapCacheOption.OnLoad;
                bi.EndInit();
                bi.Freeze();
                return bi;
            }
        }
        private void ImageProcess()
        {
            // 1. 加载原图1
            var image1 = new Image<Bgr, byte>("bird1.png");
            PreviewImage1 = new WriteableBitmap(Bitmap2BitmapImage(image1.Bitmap));

            // 2. Image ROI区域
            var image2 = image1.Clone();
            image2.ROI = new Rectangle(50, 10, image1.Width/2, image1.Height/2);
            PreviewImage2 = new WriteableBitmap(Bitmap2BitmapImage(image2.Bitmap));

            // 3. Mat 
            var mat1 = image1.Clone().Mat;
            var mat2 = new Mat(mat1, new Rectangle(70, 50, mat1.Cols/3, mat1.Rows/4)).Clone();
            PreviewImage3 = new WriteableBitmap(Bitmap2BitmapImage(mat2.Bitmap));

            // 4. 加载被融合图像，再缩小到合适大小
            var logo = new Mat("logo.png");
            PreviewImage4 = new WriteableBitmap(Bitmap2BitmapImage(logo.Bitmap));
            var logos = new Mat();
            CvInvoke.Resize(logo, logos, new Size(), 0.1, 0.1);
            PreviewImage5 = new WriteableBitmap(Bitmap2BitmapImage(logos.Bitmap));

            // 5. 直接融合
            var mat5 = mat1.Clone();
            Mat ROI = new Mat(mat5, new Rectangle(100, 50, logos.Width, logos.Height));
            logos.CopyTo(ROI);
            PreviewImage6 = new WriteableBitmap(Bitmap2BitmapImage(mat5.Bitmap));

            // 6. 掩码融合
            var mat3 = new Mat();
            mat1.CopyTo(mat3);
            Mat ROI2 = new Mat(mat3, new Rectangle(100, 50, logos.Width, logos.Height));
            Mat mask = logos.Clone(); //以灰度模式加载
            CvInvoke.CvtColor(mask, mask, ColorConversion.Bgr2Gray);
            PreviewImage7 = new WriteableBitmap(Bitmap2BitmapImage(mask.Bitmap));
            CvInvoke.Threshold(mask, mask, 20, 255, ThresholdType.Binary);
            logos.CopyTo(ROI2, mask);
            PreviewImage8 = new WriteableBitmap(Bitmap2BitmapImage(mask.Bitmap));
            PreviewImage9 = new WriteableBitmap(Bitmap2BitmapImage(mat3.Bitmap));
            //// 3. 图像缩小
            //var image3 = new Image<Bgr, byte>(image1.Width/10,image1.Height/10);
            //CvInvoke.Resize(image1, image3, image3.Size,0.1,0.1);
            //PreviewImage3 = new WriteableBitmap(Bitmap2BitmapImage(image3.Bitmap));

            //// 4. 图像平移
            //var image4 = new Image<Bgr, byte>(image1.Size);
            //int dstRows = image1.Rows;
            //int dstCols = image1.Cols;
            //int offsetX = 50;
            //int offsetY = 50;
            //for (int i = 0; i < image1.Rows; i++) {
            //    for (int j = 0; j < image1.Cols; j++) {
            //        int x = j + offsetX;
            //        int y = i + offsetY;
            //        if (x >= 0 && x < dstCols && y >= 0 && y < dstRows){
            //            image4[y, x] = image1[i, j];
            //        }
            //    }
            //}
            //PreviewImage4 = new WriteableBitmap(Bitmap2BitmapImage(image4.Bitmap));

            //// 5. 图像旋转 //也可以使用 rotate
            //var image5 = new Image<Bgr, byte>(image1.Size);
            //PointF center = new PointF(image1.Cols/2,image1.Rows/2);
            //double angle = 45;
            //double scale = 0.5;
            //// 5.1 得到旋转矩阵
            //var rotateMat = new Mat();
            //CvInvoke.GetRotationMatrix2D(center,angle,scale, rotateMat);
            //// 5.2 进行仿射变换
            //CvInvoke.WarpAffine(image1,image5, rotateMat, new Size(image1.Cols,image1.Rows));
            //PreviewImage5 = new WriteableBitmap(Bitmap2BitmapImage(image5.Bitmap));

            //// 6. 水平翻转
            //var image6 = new Image<Bgr, byte>(image1.Size);
            //CvInvoke.Flip(image1, image6, FlipType.Horizontal);
            //PreviewImage6 = new WriteableBitmap(Bitmap2BitmapImage(image6.Bitmap));

            //// 7. 图像转置
            //var image7 = new Image<Bgr, byte>(image1.Rows, image1.Cols);
            //CvInvoke.Transpose(image1, image7);
            //PreviewImage7 = new WriteableBitmap(Bitmap2BitmapImage(image7.Bitmap));

            //// 8. 图像重映射
            //Image<Gray, Single> xMatImg = new Image<Gray, Single>(image1.Cols, image1.Rows);
            //Image<Gray, Single> yMatImg = new Image<Gray, Single>(image1.Cols, image1.Rows);
            //for (int i = 0; i < image1.Rows; i++)
            //{
            //    for (int j = 0; j < image1.Cols; j++)
            //    {
            //        xMatImg[i, j] = new Gray(j);
            //        yMatImg[i, j] = new Gray(i + 10 * Math.Sin(j / 10.0));
            //    }
            //}

            //var image8 = new Image<Bgr, byte>(image1.Size);
            //CvInvoke.Remap(image1, image8, xMatImg, yMatImg, Inter.Area);
            //PreviewImage8 = new WriteableBitmap(Bitmap2BitmapImage(image8.Bitmap));

            //// 9. 垂直翻转
            //var image9 = new Image<Bgr, byte>(image1.Size);
            //CvInvoke.Flip(image1, image9, FlipType.Vertical);
            //PreviewImage9 = new WriteableBitmap(Bitmap2BitmapImage(image9.Bitmap));
        }
    }
}
