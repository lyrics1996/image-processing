﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Media.Imaging;

namespace Demo
{
    public class MainViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private WriteableBitmap _previewImage1;

        public WriteableBitmap PreviewImage1
        {
            get { return _previewImage1; }
            set { _previewImage1 = value; PropertyChanged?.Invoke(this,new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage2;

        public WriteableBitmap PreviewImage2
        {
            get { return _previewImage2; }
            set { _previewImage2 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage3;

        public WriteableBitmap PreviewImage3
        {
            get { return _previewImage3; }
            set { _previewImage3 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage4;

        public WriteableBitmap PreviewImage4
        {
            get { return _previewImage4; }
            set { _previewImage4 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }


        private WriteableBitmap _previewImage5;

        public WriteableBitmap PreviewImage5
        {
            get { return _previewImage5; }
            set { _previewImage5 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage6;

        public WriteableBitmap PreviewImage6
        {
            get { return _previewImage6; }
            set { _previewImage6 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage7;

        public WriteableBitmap PreviewImage7
        {
            get { return _previewImage7; }
            set { _previewImage7 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }
        private WriteableBitmap _previewImage8;

        public WriteableBitmap PreviewImage8
        {
            get { return _previewImage8; }
            set { _previewImage8 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }
        private WriteableBitmap _previewImage9;

        public WriteableBitmap PreviewImage9
        {
            get { return _previewImage9; }
            set { _previewImage9 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }
        public MainViewModel() {
            ImageProcess();
        }

        private BitmapImage Bitmap2BitmapImage(Bitmap bitmap ) {
            using (MemoryStream ms = new MemoryStream()) {
                bitmap.Save(ms,ImageFormat.Bmp);
                BitmapImage bi = new BitmapImage();
                bi.BeginInit();
                bi.StreamSource = ms;
                bi.CacheOption = BitmapCacheOption.OnLoad;
                bi.EndInit();
                bi.Freeze();
                return bi;
            }
        }
        private Bitmap Text(Bitmap img,string str) {
            Bitmap bmp = new Bitmap(img.Width, img.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            using (Graphics g = Graphics.FromImage(bmp))
            {
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                g.DrawImage(img, 0, 0, bmp.Width, bmp.Height);
                g.DrawString(str, new Font("Verdana", 100), new SolidBrush(Color.Black), new PointF(0, 0));
            }
            return bmp;

        }
        private Bitmap Text2(Bitmap img, string str)
        {
            Bitmap bmp = new Bitmap(img.Width, img.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            using (Graphics g = Graphics.FromImage(bmp))
            {
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                g.DrawImage(img, 0, 0, bmp.Width, bmp.Height);
                g.DrawString(str, new Font("Verdana", 50), new SolidBrush(Color.White), new PointF(0, 0));
            }
            return bmp;

        }

        private Bitmap Text3(Bitmap img, string str)
        {
            Bitmap bmp = new Bitmap(img.Width, img.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            using (Graphics g = Graphics.FromImage(bmp))
            {
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                g.DrawImage(img, 0, 0, bmp.Width, bmp.Height);
                g.DrawString(str, new Font("Verdana", 10), new SolidBrush(Color.Black), new PointF(0, 0));
            }
            return bmp;

        }
        private void ImageProcess()
        {
            //// 1. 加载原图
            //var image1 = new Image<Bgr, byte>("Test.png");
            //var image0 = image1.Mat.Clone();
            //var imgGray = new Mat();
            //CvInvoke.CvtColor(image0,imgGray,ColorConversion.Bgr2Gray);
            //PreviewImage1 = new WriteableBitmap(Bitmap2BitmapImage(Text(image1.Bitmap, "原图")));

            //// 2. 加载模板
            //var img3 = new Mat("testTemplate.png",0);
            //PreviewImage2 = new WriteableBitmap(Bitmap2BitmapImage(Text3(img3.Bitmap, "模板")));

            //// 3. Sqdiff取最小值
            //var res = new Mat();
            //CvInvoke.MatchTemplate(imgGray, img3, res, TemplateMatchingType.CcoeffNormed);
            //var img4 = image0.Clone();
            //var m = new Matrix<float>(res.Rows, res.Cols);
            //res.CopyTo(m);
            //var image = res.ToImage<Gray, byte>();
            //for (int i = 0; i < res.Rows; i++)
            //{
            //    for (int j = 0; j < res.Cols; j++)
            //    {
            //        if (m[i, j] > 0.8)
            //        {
            //            CvInvoke.Rectangle(img4, new Rectangle(new Point(j, i), img3.Size), new MCvScalar(0, 0, 255), 2);
            //        }
            //    }
            //}

            //PreviewImage3 = new WriteableBitmap(Bitmap2BitmapImage(Text(img4.Bitmap, "结果")));

            //  1.加载原图
            var image1 = new Image<Bgr, byte>("bird1.png");
            var image0 = image1.Mat.Clone();
            var imgGray = new Mat();
            CvInvoke.CvtColor(image0, imgGray, ColorConversion.Bgr2Gray);
            PreviewImage1 = new WriteableBitmap(Bitmap2BitmapImage(Text(image1.Bitmap, "原图")));

            // 2. 原图转灰度
            PreviewImage2 = new WriteableBitmap(Bitmap2BitmapImage(Text(imgGray.Bitmap, "灰度")));

            // 3. 加载模板
            var img3 = new Mat("birdTemplate.png", 0);
            PreviewImage3 = new WriteableBitmap(Bitmap2BitmapImage(Text3(img3.Bitmap, "模板")));
            // 需要用到的一些参数
            var res = new Mat();
            double minLoc = 0, maxLoc = 0;
            Point minPoint = new Point();
            Point maxPoint = new Point();

            // 4. Sqdiff取最小值
            CvInvoke.MatchTemplate(imgGray, img3, res, TemplateMatchingType.Sqdiff);
            CvInvoke.MinMaxLoc(res, ref minLoc, ref maxLoc, ref minPoint, ref maxPoint);
            var img4 = image0.Clone();
            CvInvoke.Rectangle(img4, new Rectangle(minPoint, img3.Size), new MCvScalar(0, 0, 255), 2);
            PreviewImage4 = new WriteableBitmap(Bitmap2BitmapImage(Text(img4.Bitmap, "Sqdiff")));

            // 5 .SqdiffNormed
            CvInvoke.MatchTemplate(imgGray, img3, res, TemplateMatchingType.SqdiffNormed);
            CvInvoke.MinMaxLoc(res, ref minLoc, ref maxLoc, ref minPoint, ref maxPoint);
            var img5 = image0.Clone();
            CvInvoke.Rectangle(img5, new Rectangle(minPoint, img3.Size), new MCvScalar(0, 0, 255), 2);
            PreviewImage7 = new WriteableBitmap(Bitmap2BitmapImage(Text(img5.Bitmap, "SqdiffNormed")));

            // 6 .Ccoeff
            CvInvoke.MatchTemplate(imgGray, img3, res, TemplateMatchingType.Ccoeff);
            CvInvoke.MinMaxLoc(res, ref minLoc, ref maxLoc, ref minPoint, ref maxPoint);
            var img6 = image0.Clone();
            CvInvoke.Rectangle(img6, new Rectangle(maxPoint, img3.Size), new MCvScalar(0, 0, 255), 2);
            PreviewImage5 = new WriteableBitmap(Bitmap2BitmapImage(Text(img6.Bitmap, "Ccoeff")));

            // 7 .CcoeffNormed
            CvInvoke.MatchTemplate(imgGray, img3, res, TemplateMatchingType.CcoeffNormed);
            CvInvoke.MinMaxLoc(res, ref minLoc, ref maxLoc, ref minPoint, ref maxPoint);
            var img7 = image0.Clone();
            CvInvoke.Rectangle(img7, new Rectangle(maxPoint, img3.Size), new MCvScalar(0, 0, 255), 2);
            PreviewImage8 = new WriteableBitmap(Bitmap2BitmapImage(Text(img7.Bitmap, "CcoeffNormed")));


            // 8 .Ccorr
            CvInvoke.MatchTemplate(imgGray, img3, res, TemplateMatchingType.Ccorr);
            CvInvoke.MinMaxLoc(res, ref minLoc, ref maxLoc, ref minPoint, ref maxPoint);
            var img8 = image0.Clone();
            CvInvoke.Rectangle(img8, new Rectangle(maxPoint, img3.Size), new MCvScalar(0, 0, 255), 2);
            PreviewImage6 = new WriteableBitmap(Bitmap2BitmapImage(Text(img8.Bitmap, "Ccorr")));


            // 9 .CcorrNormed
            CvInvoke.MatchTemplate(imgGray, img3, res, TemplateMatchingType.CcorrNormed);
            CvInvoke.MinMaxLoc(res, ref minLoc, ref maxLoc, ref minPoint, ref maxPoint);
            var img9 = image0.Clone();
            CvInvoke.Rectangle(img9, new Rectangle(maxPoint, img3.Size), new MCvScalar(0, 0, 255), 2);
            PreviewImage9 = new WriteableBitmap(Bitmap2BitmapImage(Text(img9.Bitmap, "CcorrNormed")));







            //// 2. 灰度图
            //var img2 = image0.Clone();
            //CvInvoke.CvtColor(image0, img2, ColorConversion.Bgr2Gray);
            //PreviewImage2 = new WriteableBitmap(Bitmap2BitmapImage(Text(img2.Bitmap, "灰度图")));

            //// 3. 二值化
            //var img3 = new Mat();
            //CvInvoke.Threshold(img2, img3, 0, 255, ThresholdType.Otsu);
            //PreviewImage3 = new WriteableBitmap(Bitmap2BitmapImage(Text2(img3.Bitmap, "Otsu")));

            //// 4. 查找轮廓
            //VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint();
            //Mat hierarchy = new Mat();
            //CvInvoke.FindContours(img3, contours, hierarchy, RetrType.Tree, ChainApproxMethod.ChainApproxNone);

            //// 5. 绘制所有轮廓
            //var img4 = image0.Clone();
            //CvInvoke.DrawContours(img4, contours, -1, new MCvScalar(0, 0, 255), 2);
            //PreviewImage4 = new WriteableBitmap(Bitmap2BitmapImage(Text(img4.Bitmap, "绘制所有轮廓")));

            //// 6. 绘制最大的轮廓
            //double MaxArea = 0;
            //int maxIndex = 0;
            //for (int i = 0; i < contours.Size; i++) {
            //    var area = CvInvoke.ContourArea(contours[i]);
            //    if (area > MaxArea) {
            //        MaxArea = area;
            //        maxIndex = i;
            //    }
            //}
            //var img5 = image0.Clone();
            //CvInvoke.DrawContours(img5, contours, maxIndex, new MCvScalar(0, 0, 255), 2);
            //PreviewImage5 = new WriteableBitmap(Bitmap2BitmapImage(Text(img5.Bitmap, "绘制面积最大的轮廓")));

            //// 7. 轮廓近似
            //var e = CvInvoke.ArcLength(contours[maxIndex],true)*0.01;
            //VectorOfPoint s = new VectorOfPoint();
            //CvInvoke.ApproxPolyDP(contours[maxIndex],s,e,true);
            //var img6 = image0.Clone();
            //VectorOfVectorOfPoint contours2 = new VectorOfVectorOfPoint();
            //contours2.Push(s);
            //CvInvoke.DrawContours(img6, contours2, -1, new MCvScalar(0, 0, 255), 2);
            //PreviewImage6 = new WriteableBitmap(Bitmap2BitmapImage(Text(img6.Bitmap, "绘制近似后的轮廓")));

            //// 8. 边界矩形
            //var rect =  CvInvoke.BoundingRectangle(contours[maxIndex]);
            //var img7 = image0.Clone();
            //CvInvoke.Rectangle(img7, rect, new MCvScalar(0, 0, 255), 2);
            //PreviewImage7 = new WriteableBitmap(Bitmap2BitmapImage(Text(img7.Bitmap, "绘制边界矩形")));

            //// 9. 外接圆
            //var img8 = image0.Clone();
            //var circle = CvInvoke.MinEnclosingCircle(contours[maxIndex]);
            //CvInvoke.Circle(img8, new Point((int)circle.Center.X, (int)circle.Center.Y), (int)circle.Radius, new MCvScalar(0, 0, 255), 2);
            //PreviewImage8 = new WriteableBitmap(Bitmap2BitmapImage(Text(img8.Bitmap, "绘制外接圆")));

            //// 10. 外接三角形
            //var img9 = image0.Clone();
            //VectorOfPoint triangle = new VectorOfPoint();
            //CvInvoke.MinEnclosingTriangle(contours[maxIndex], triangle);
            //CvInvoke.Line(img9, triangle[0],triangle[1], new MCvScalar(0, 0, 255), 2);
            //CvInvoke.Line(img9, triangle[1], triangle[2], new MCvScalar(0, 0, 255), 2);
            //CvInvoke.Line(img9, triangle[2], triangle[0], new MCvScalar(0, 0, 255), 2);
            //PreviewImage9 = new WriteableBitmap(Bitmap2BitmapImage(Text(img9.Bitmap, "绘制外接三角形")));
            //// 5. Canny
            //Mat image4 = new Mat();
            //CvInvoke.Canny(image3.Clone(),image4,50,150);
            //PreviewImage5 = new WriteableBitmap(Bitmap2BitmapImage(Text2(image4.Bitmap, "Canny 50,150")));

            //// 6. Canny
            //Mat image6 = new Mat();
            //CvInvoke.Canny(image3.Clone(), image6, 80, 100);
            //PreviewImage6 = new WriteableBitmap(Bitmap2BitmapImage(Text2(image6.Bitmap, "Canny 80,100")));

            //// 7. Canny
            //Mat image7 = new Mat();
            //CvInvoke.Canny(image3.Clone(), image7, 80, 150);
            //PreviewImage7 = new WriteableBitmap(Bitmap2BitmapImage(Text2(image7.Bitmap, "Canny 80,150")));

            //// 8. Canny
            //Mat image8 = new Mat();
            //CvInvoke.Canny(image3.Clone(), image8, 100, 150);
            //PreviewImage8 = new WriteableBitmap(Bitmap2BitmapImage(Text2(image8.Bitmap, "Canny 100,150")));

            //// 9. Canny
            //Mat image9 = new Mat();
            //CvInvoke.Canny(image3.Clone(), image9, 50, 80);
            //PreviewImage9 = new WriteableBitmap(Bitmap2BitmapImage(Text2(image9.Bitmap, "Canny 50,80")));
        }
    }
}
