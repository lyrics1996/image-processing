﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Media.Imaging;

namespace Demo
{
    public class MainViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private WriteableBitmap _previewImage1;

        public WriteableBitmap PreviewImage1
        {
            get { return _previewImage1; }
            set { _previewImage1 = value; PropertyChanged?.Invoke(this,new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage2;

        public WriteableBitmap PreviewImage2
        {
            get { return _previewImage2; }
            set { _previewImage2 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage3;

        public WriteableBitmap PreviewImage3
        {
            get { return _previewImage3; }
            set { _previewImage3 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage4;

        public WriteableBitmap PreviewImage4
        {
            get { return _previewImage4; }
            set { _previewImage4 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }


        private WriteableBitmap _previewImage5;

        public WriteableBitmap PreviewImage5
        {
            get { return _previewImage5; }
            set { _previewImage5 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage6;

        public WriteableBitmap PreviewImage6
        {
            get { return _previewImage6; }
            set { _previewImage6 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage7;

        public WriteableBitmap PreviewImage7
        {
            get { return _previewImage7; }
            set { _previewImage7 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }
        private WriteableBitmap _previewImage8;

        public WriteableBitmap PreviewImage8
        {
            get { return _previewImage8; }
            set { _previewImage8 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }
        private WriteableBitmap _previewImage9;

        public WriteableBitmap PreviewImage9
        {
            get { return _previewImage9; }
            set { _previewImage9 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }
        public MainViewModel() {
            ImageProcess();
        }

        private BitmapImage Bitmap2BitmapImage(Bitmap bitmap ) {
            using (MemoryStream ms = new MemoryStream()) {
                bitmap.Save(ms,ImageFormat.Bmp);
                BitmapImage bi = new BitmapImage();
                bi.BeginInit();
                bi.StreamSource = ms;
                bi.CacheOption = BitmapCacheOption.OnLoad;
                bi.EndInit();
                bi.Freeze();
                return bi;
            }
        }
        private Bitmap Text(Bitmap img,string str) {
            Bitmap bmp = new Bitmap(img.Width, img.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            using (Graphics g = Graphics.FromImage(bmp))
            {
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                g.DrawImage(img, 0, 0, bmp.Width, bmp.Height);
                g.DrawString(str, new Font("Verdana", 50), new SolidBrush(Color.Black), new PointF(100, 0));
            }
            return bmp;

        }
        private Bitmap Text2(Bitmap img, string str)
        {
            Bitmap bmp = new Bitmap(img.Width, img.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            using (Graphics g = Graphics.FromImage(bmp))
            {
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                g.DrawImage(img, 0, 0, bmp.Width, bmp.Height);
                g.DrawString(str, new Font("Verdana", 50), new SolidBrush(Color.White), new PointF(100, 0));
            }
            return bmp;

        }
        private void ImageProcess()
        {
            // 1. 加载原图
            var image1 = new Image<Bgr, byte>("bird1.png");
            var image0 = image1.Mat.Clone();
            PreviewImage1 = new WriteableBitmap(Bitmap2BitmapImage(Text(image1.Bitmap, "原图")));

            // 2. 转为灰度（当然也可以直接读取为灰度）
            Mat image2 = new Mat();
            CvInvoke.CvtColor(image0.Clone(), image2, ColorConversion.Bgr2Gray);
            PreviewImage2 = new WriteableBitmap(Bitmap2BitmapImage(Text(image2.Bitmap, "灰度")));

            // 3. 自动阈值
            Mat image3 = new Mat();
            CvInvoke.Threshold(image2, image3, 0, 255, ThresholdType.Otsu);
            PreviewImage3 = new WriteableBitmap(Bitmap2BitmapImage(Text(image3.Bitmap, "Otsu阈值")));

            // 4. sobel算子
            Mat image4 = new Mat();
            CvInvoke.Sobel(image3.Clone(), image4, DepthType.Cv64F, 1, 0, 3);
            CvInvoke.ConvertScaleAbs(image4, image4, 1.0, 0);
            Mat image5 = new Mat();
            CvInvoke.Sobel(image3.Clone(), image5, DepthType.Cv64F, 0, 1, 3);
            CvInvoke.ConvertScaleAbs(image5, image5, 1.0, 0);
            var image6 = new Mat();
            CvInvoke.AddWeighted(image4, 0.5, image5, 0.5, 0, image6);
            PreviewImage4 = new WriteableBitmap(Bitmap2BitmapImage(Text2(image6.Bitmap, "sobel算子")));

            // 5. Scharr算子
            Mat image7 = new Mat();
            Mat image9 = new Mat();
            CvInvoke.Scharr(image3.Clone(),image7,DepthType.Cv64F,1,0,3);
            CvInvoke.Scharr(image3.Clone(), image9, DepthType.Cv64F, 0, 1, 3);
            CvInvoke.ConvertScaleAbs(image7, image7, 1.0, 0);
            CvInvoke.ConvertScaleAbs(image9, image9, 1.0, 0);
            var image10 = new Mat();
            CvInvoke.AddWeighted(image7, 0.5, image9, 0.5, 0, image10);
            PreviewImage5 = new WriteableBitmap(Bitmap2BitmapImage(Text2(image10.Bitmap, "Scharr算子")));

            // 6. Laplacian算子
            Mat image8 = new Mat();
            CvInvoke.Laplacian(image3.Clone(), image8, DepthType.Cv64F);
            CvInvoke.ConvertScaleAbs(image8, image8, 1.0, 0);
            PreviewImage6 = new WriteableBitmap(Bitmap2BitmapImage(Text2(image8.Bitmap, "Laplacian算子")));
            //// 3. 腐蚀 将白色变小，黑色变大
            //var image3 = new Image<Bgr, byte>(image1.Size);
            //CvInvoke.Erode(image0.Clone(), image3, element, new Point(-1, -1), 1, BorderType.Default, new MCvScalar());
            //PreviewImage3 = new WriteableBitmap(Bitmap2BitmapImage(Text(image3.Bitmap, "腐蚀")));

            //// 4. 开运算（先腐蚀后膨胀）消除小物体，平滑
            //var image4 = new Image<Bgr, byte>(image1.Size);
            //CvInvoke.MorphologyEx(image0.Clone(),image4,MorphOp.Open,element,new Point(-1,-1),1,BorderType.Default,new MCvScalar());
            //PreviewImage4 = new WriteableBitmap(Bitmap2BitmapImage(Text(image4.Bitmap, "开运算")));

            //// 5. 闭运算（先膨胀后腐蚀）消除小型黑洞
            //var image5 = new Image<Bgr, byte>(image1.Size);
            //CvInvoke.MorphologyEx(image0.Clone(), image5, MorphOp.Close, element, new Point(-1, -1), 1, BorderType.Default, new MCvScalar());
            //PreviewImage5 = new WriteableBitmap(Bitmap2BitmapImage(Text(image5.Bitmap, "闭运算")));

            //// 6. 形态学梯度（膨胀图和腐蚀图之差） 保留边缘
            //var image6 = new Image<Bgr, byte>(image1.Size);
            //CvInvoke.MorphologyEx(image0.Clone(), image6, MorphOp.Gradient, element, new Point(-1, -1), 1, BorderType.Default, new MCvScalar());
            //PreviewImage6 = new WriteableBitmap(Bitmap2BitmapImage(Text(image6.Bitmap, "形态学梯度")));

            //// 7. 顶帽 （原图和开运算作差） 分离比 邻近点 亮的点
            //var image7 = new Image<Bgr, byte>(image1.Size);
            //CvInvoke.MorphologyEx(image0.Clone(), image7, MorphOp.Tophat, element, new Point(-1, -1), 1, BorderType.Default, new MCvScalar());
            //PreviewImage7 = new WriteableBitmap(Bitmap2BitmapImage(Text(image7.Bitmap, "顶帽")));

            //// 8. 黑帽 （闭运算和原图作差） 分离比 邻近点 暗的点
            //var image8 = new Image<Bgr, byte>(image1.Size);
            //CvInvoke.MorphologyEx(image0.Clone(), image8, MorphOp.Blackhat, element, new Point(-1, -1), 1, BorderType.Default, new MCvScalar());
            //PreviewImage8 = new WriteableBitmap(Bitmap2BitmapImage(Text(image8.Bitmap, "黑帽")));

            //// 9. 一个没什么意义的转换
            //var image9 = new Image<Bgr, byte>(image1.Size);
            //var image10 = new Image<Bgr, byte>(image1.Size);
            //CvInvoke.MorphologyEx(image0.Clone(), image9, MorphOp.Tophat, element, new Point(-1, -1), 1, BorderType.Default, new MCvScalar());
            //CvInvoke.BitwiseNot(image9,image9);
            //CvInvoke.BitwiseXor(image9,image0.Clone(),image10);
            //var bm = image10.Bitmap;
            //Graphics g = Graphics.FromImage(bm);
            //g.DrawString("顶帽+取反+异或", new Font("Verdana", 20), new SolidBrush(Color.Black), new PointF(20, 0));
            //PreviewImage9 = new WriteableBitmap(Bitmap2BitmapImage(bm));

        }
    }
}
