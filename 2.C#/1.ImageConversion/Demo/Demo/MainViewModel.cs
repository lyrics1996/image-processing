﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace Demo
{
    public class MainViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private WriteableBitmap _previewImage1;

        public WriteableBitmap PreviewImage1
        {
            get { return _previewImage1; }
            set { _previewImage1 = value; PropertyChanged?.Invoke(this,new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage2;

        public WriteableBitmap PreviewImage2
        {
            get { return _previewImage2; }
            set { _previewImage2 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage3;

        public WriteableBitmap PreviewImage3
        {
            get { return _previewImage3; }
            set { _previewImage3 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage4;

        public WriteableBitmap PreviewImage4
        {
            get { return _previewImage4; }
            set { _previewImage4 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }


        private WriteableBitmap _previewImage5;

        public WriteableBitmap PreviewImage5
        {
            get { return _previewImage5; }
            set { _previewImage5 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage6;

        public WriteableBitmap PreviewImage6
        {
            get { return _previewImage6; }
            set { _previewImage6 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        public MainViewModel() {
            ImageProcess();
        }

        private BitmapImage Bitmap2BitmapImage(Bitmap bitmap ) {
            using (MemoryStream ms = new MemoryStream()) {
                bitmap.Save(ms,ImageFormat.Bmp);
                BitmapImage bi = new BitmapImage();
                bi.BeginInit();
                bi.StreamSource = ms;
                bi.CacheOption = BitmapCacheOption.OnLoad;
                bi.EndInit();
                bi.Freeze();
                return bi;
            }
        }
        private void ImageProcess()
        {
            // 1. 加载原图
            Mat image1 = CvInvoke.Imread("bird.png");
            PreviewImage1 = new WriteableBitmap(Bitmap2BitmapImage(image1.Bitmap));
            // 2. 转为灰度图
            Mat image2 = new Mat();
            CvInvoke.CvtColor(image1,image2,ColorConversion.Rgb2Gray);
            PreviewImage2 = new WriteableBitmap(Bitmap2BitmapImage(image2.Bitmap));
            // 3.反色
            Mat image3= new Mat();
            CvInvoke.CvtColor(image1, image3, ColorConversion.Rgb2Bgr);
            PreviewImage3 = new WriteableBitmap(Bitmap2BitmapImage(image3.Bitmap));
            // 4. 区分RGB三色通道
            Image<Rgb, byte> imageTemp = image1.ToImage<Rgb, byte>();
            var channels = imageTemp.Split();
            Image<Gray, byte> imageEmpty = new Image<Gray, byte>(imageTemp.Size);
            // 4.1 红
            Image<Rgb, byte> image6 = new Image<Rgb, byte>(new Image<Gray, byte>[] { channels[0], imageEmpty, imageEmpty });
            PreviewImage6 = new WriteableBitmap(Bitmap2BitmapImage(image6.Bitmap));
            // 4.2 绿
            Image<Rgb, byte> image4 = new Image<Rgb, byte>(new Image<Gray, byte>[] { imageEmpty, channels[1], imageEmpty });
            PreviewImage4 = new WriteableBitmap(Bitmap2BitmapImage(image4.Bitmap));
            // 4.3 蓝
            Image<Rgb, byte> image5 = new Image<Rgb, byte>(new Image<Gray, byte>[] { imageEmpty, imageEmpty, channels[2] });
            PreviewImage5 = new WriteableBitmap(Bitmap2BitmapImage(image5.Bitmap));
        
        }
    }
}
