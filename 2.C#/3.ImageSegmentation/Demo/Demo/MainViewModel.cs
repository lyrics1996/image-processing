﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace Demo
{
    public class MainViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private WriteableBitmap _previewImage1;

        public WriteableBitmap PreviewImage1
        {
            get { return _previewImage1; }
            set { _previewImage1 = value; PropertyChanged?.Invoke(this,new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage2;

        public WriteableBitmap PreviewImage2
        {
            get { return _previewImage2; }
            set { _previewImage2 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage3;

        public WriteableBitmap PreviewImage3
        {
            get { return _previewImage3; }
            set { _previewImage3 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage4;

        public WriteableBitmap PreviewImage4
        {
            get { return _previewImage4; }
            set { _previewImage4 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }


        private WriteableBitmap _previewImage5;

        public WriteableBitmap PreviewImage5
        {
            get { return _previewImage5; }
            set { _previewImage5 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage6;

        public WriteableBitmap PreviewImage6
        {
            get { return _previewImage6; }
            set { _previewImage6 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        public MainViewModel() {
            ImageProcess();
        }

        private BitmapImage Bitmap2BitmapImage(Bitmap bitmap ) {
            using (MemoryStream ms = new MemoryStream()) {
                bitmap.Save(ms,ImageFormat.Bmp);
                BitmapImage bi = new BitmapImage();
                bi.BeginInit();
                bi.StreamSource = ms;
                bi.CacheOption = BitmapCacheOption.OnLoad;
                bi.EndInit();
                bi.Freeze();
                return bi;
            }
        }
        private void ImageProcess()
        {
            // 1. 加载原图
            var image1 = new Image<Bgr, byte>("bird1.png");
            PreviewImage1 = new WriteableBitmap(Bitmap2BitmapImage(image1.Bitmap));

            // 2. 均值滤波
            var image2 = new Image<Bgr, byte>(image1.Size);
            CvInvoke.Blur(image1, image2, new Size(10, 10), new Point(-1, -1));
            PreviewImage2 = new WriteableBitmap(Bitmap2BitmapImage(image2.Bitmap));

            // 3.色彩空间转换
            var image3 = new Image<Hsv, byte>(image1.Size);
            CvInvoke.CvtColor(image2, image3, ColorConversion.Bgr2Hsv);
            PreviewImage3 = new WriteableBitmap(Bitmap2BitmapImage(image3.Bitmap));

            // 4.提取阈值
            var min_blue = new Hsv(55, 0, 0);
            var max_blue = new Hsv(118, 255, 255);
            var image4 = image3.InRange(min_blue, max_blue);
            PreviewImage4 = new WriteableBitmap(Bitmap2BitmapImage(image4.Bitmap));

            // 5. 转换颜色空间

            var image5 = new Image<Bgr, byte>(image1.Size);
            CvInvoke.CvtColor(image4, image5, ColorConversion.Gray2Bgr);
            PreviewImage5 = new WriteableBitmap(Bitmap2BitmapImage(image5.Bitmap));

            // 6. 通过与运算混合
            var image6 = new Image<Bgr, byte>(image1.Size);
            CvInvoke.BitwiseAnd(image5, image1, image6);
            PreviewImage6 = new WriteableBitmap(Bitmap2BitmapImage(image6.Bitmap));

        }
    }
}
