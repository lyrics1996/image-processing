﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Media.Imaging;

namespace Demo
{
    public class MainViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private WriteableBitmap _previewImage1;

        public WriteableBitmap PreviewImage1
        {
            get { return _previewImage1; }
            set { _previewImage1 = value; PropertyChanged?.Invoke(this,new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage2;

        public WriteableBitmap PreviewImage2
        {
            get { return _previewImage2; }
            set { _previewImage2 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage3;

        public WriteableBitmap PreviewImage3
        {
            get { return _previewImage3; }
            set { _previewImage3 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage4;

        public WriteableBitmap PreviewImage4
        {
            get { return _previewImage4; }
            set { _previewImage4 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }


        private WriteableBitmap _previewImage5;

        public WriteableBitmap PreviewImage5
        {
            get { return _previewImage5; }
            set { _previewImage5 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage6;

        public WriteableBitmap PreviewImage6
        {
            get { return _previewImage6; }
            set { _previewImage6 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage7;

        public WriteableBitmap PreviewImage7
        {
            get { return _previewImage7; }
            set { _previewImage7 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }
        private WriteableBitmap _previewImage8;

        public WriteableBitmap PreviewImage8
        {
            get { return _previewImage8; }
            set { _previewImage8 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }
        private WriteableBitmap _previewImage9;

        public WriteableBitmap PreviewImage9
        {
            get { return _previewImage9; }
            set { _previewImage9 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }
        public MainViewModel() {
            ImageProcess();
        }

        private BitmapImage Bitmap2BitmapImage(Bitmap bitmap ) {
            using (MemoryStream ms = new MemoryStream()) {
                bitmap.Save(ms,ImageFormat.Bmp);
                BitmapImage bi = new BitmapImage();
                bi.BeginInit();
                bi.StreamSource = ms;
                bi.CacheOption = BitmapCacheOption.OnLoad;
                bi.EndInit();
                bi.Freeze();
                return bi;
            }
        }
        private Bitmap Text(Bitmap img,string str) {
            Bitmap bmp = new Bitmap(img.Width, img.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            using (Graphics g = Graphics.FromImage(bmp))
            {
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                g.DrawImage(img, 0, 0, bmp.Width, bmp.Height);
                g.DrawString(str, new Font("Verdana", 100), new SolidBrush(Color.Black), new PointF(0, 0));
            }
            return bmp;

        }
        private Bitmap Text2(Bitmap img, string str)
        {
            Bitmap bmp = new Bitmap(img.Width, img.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            using (Graphics g = Graphics.FromImage(bmp))
            {
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                g.DrawImage(img, 0, 0, bmp.Width, bmp.Height);
                g.DrawString(str, new Font("Verdana", 50), new SolidBrush(Color.White), new PointF(0, 0));
            }
            return bmp;

        }

        private Bitmap Text3(Bitmap img, string str)
        {
            Bitmap bmp = new Bitmap(img.Width, img.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            using (Graphics g = Graphics.FromImage(bmp))
            {
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                g.DrawImage(img, 0, 0, bmp.Width, bmp.Height);
                g.DrawString(str, new Font("Verdana", 10), new SolidBrush(Color.Black), new PointF(0, 0));
            }
            return bmp;

        }
        private void ImageProcess()
        {
            //  1.加载原图
            var image1 = new Image<Bgr, byte>("bird1.png");
            var image0 = image1.Mat.Clone();
            PreviewImage1 = new WriteableBitmap(Bitmap2BitmapImage(Text(image1.Bitmap, "原图")));

            // 2. 原图转灰度
            var imgGray = new Mat();
            CvInvoke.CvtColor(image0, imgGray, ColorConversion.Bgr2Gray);
            PreviewImage2 = new WriteableBitmap(Bitmap2BitmapImage(Text(imgGray.Bitmap, "灰度")));

            // 3. 傅里叶变换前需要转换为float型，且有实部和虚部
            var imgR= imgGray.Clone();
            imgR.ConvertTo(imgR,DepthType.Cv32F);
            var imgI = Mat.Zeros(imgGray.Rows,imgGray.Cols,DepthType.Cv32F,1);
            VectorOfMat vectorOfMat = new VectorOfMat();
            vectorOfMat.Push(imgR);
            vectorOfMat.Push(imgI);
            var img = new Mat(imgR.Size,DepthType.Cv32F,2);
            CvInvoke.Merge(vectorOfMat,img);

            // 4. 傅里叶变换
            Mat fourier = new Mat(imgR.Size, DepthType.Cv32F, 2);
            CvInvoke.Dft(img, fourier,DxtType.Forward,imgR.Rows);

            // 5. 将复数转换为振幅 log(1+sqrt(Re(DFT(I)^2+Im(DFT(I)^2))
            Mat magnitudeImage = Magnitude(fourier);

            // 6. 将频率低的显示在中心
            SwitchQuadrants(ref magnitudeImage);

            // 7. 归一化之后才能显示
            var img7 = magnitudeImage.Clone(); 
            CvInvoke.Normalize(magnitudeImage, magnitudeImage, 0, 255, NormType.MinMax, DepthType.Cv8U);
            PreviewImage3 = new WriteableBitmap(Bitmap2BitmapImage(Text(magnitudeImage.Bitmap, "傅里叶变换")));

         

        }


        //将复数转换为幅值，并进行对数尺度缩放
        private Mat Magnitude(Mat fftData)
        {
            //傅里叶变换的实部
            Mat Real = new Mat(fftData.Size, DepthType.Cv32F, 1);
            //傅里叶变换的虚部
            Mat Imaginary = new Mat(fftData.Size, DepthType.Cv32F, 1);
            VectorOfMat channels = new VectorOfMat();
            CvInvoke.Split(fftData, channels); //将多通道mat分离成几个单通道mat
            Real = channels.GetOutputArray().GetMat(0);
            Imaginary = channels.GetOutputArray().GetMat(1);
            CvInvoke.Pow(Real, 2.0, Real);
            CvInvoke.Pow(Imaginary, 2.0, Imaginary);
            CvInvoke.Add(Real, Imaginary, Real);
            CvInvoke.Pow(Real, 0.5, Real);
            Mat onesMat = Mat.Ones(Real.Rows, Real.Cols, DepthType.Cv32F, 1);
            CvInvoke.Add(Real, onesMat, Real);
            CvInvoke.Log(Real, Real); //求自然对数
            return Real;
        }

        //重新排列傅里叶图像中的象限，使得原点位于图像中心
        private void SwitchQuadrants(ref Mat mat)
        {
            int cx = mat.Cols / 2;
            int cy = mat.Rows / 2;
            Mat q0 = new Mat(mat, new Rectangle(0, 0, cx, cy)); //ROI左上区域
            Mat q1 = new Mat(mat, new Rectangle(cx, 0, cx, cy)); //ROI右上区域
            Mat q2 = new Mat(mat, new Rectangle(0, cy, cx, cy)); //ROI左下区域
            Mat q3 = new Mat(mat, new Rectangle(cx, cy, cx, cy)); //ROI右下区域
            Mat temp = new Mat(q0.Size, DepthType.Cv32F, 1);
            //交换象限（左上和右下进行交换）
            q0.CopyTo(temp);
            q3.CopyTo(q0);
            temp.CopyTo(q3);
            //交换象限（右上和左下进行交换）
            q1.CopyTo(temp);
            q2.CopyTo(q1);
            temp.CopyTo(q2);
        }
    }
}
