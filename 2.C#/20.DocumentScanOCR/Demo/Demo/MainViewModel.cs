﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Threading;
using System.Windows.Media.Imaging;
using Tesseract;

namespace Demo
{
    public class MainViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private WriteableBitmap _previewImage1;

        public WriteableBitmap PreviewImage1
        {
            get { return _previewImage1; }
            set { _previewImage1 = value; PropertyChanged?.Invoke(this,new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage2;

        public WriteableBitmap PreviewImage2
        {
            get { return _previewImage2; }
            set { _previewImage2 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage3;

        public WriteableBitmap PreviewImage3
        {
            get { return _previewImage3; }
            set { _previewImage3 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage4;

        public WriteableBitmap PreviewImage4
        {
            get { return _previewImage4; }
            set { _previewImage4 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }


        private WriteableBitmap _previewImage5;

        public WriteableBitmap PreviewImage5
        {
            get { return _previewImage5; }
            set { _previewImage5 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage6;

        public WriteableBitmap PreviewImage6
        {
            get { return _previewImage6; }
            set { _previewImage6 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage7;

        public WriteableBitmap PreviewImage7
        {
            get { return _previewImage7; }
            set { _previewImage7 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }
        private WriteableBitmap _previewImage8;

        public WriteableBitmap PreviewImage8
        {
            get { return _previewImage8; }
            set { _previewImage8 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }
        private WriteableBitmap _previewImage9;

        public WriteableBitmap PreviewImage9
        {
            get { return _previewImage9; }
            set { _previewImage9 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }
        public MainViewModel() {
            ImageProcess();
        }

        private BitmapImage Bitmap2BitmapImage(Bitmap bitmap ) {
            using (MemoryStream ms = new MemoryStream()) {
                bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                BitmapImage bi = new BitmapImage();
                bi.BeginInit();
                bi.StreamSource = ms;
                bi.CacheOption = BitmapCacheOption.OnLoad;
                bi.EndInit();
                bi.Freeze();
                return bi;
            }
        }
        private Bitmap Text(Bitmap img,string str) {
            Bitmap bmp = new Bitmap(img.Width, img.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            using (Graphics g = Graphics.FromImage(bmp))
            {
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                g.DrawImage(img, 0, 0, bmp.Width, bmp.Height);
                g.DrawString(str, new Font("Verdana", 100), new SolidBrush(Color.Black), new PointF(0, 0));
            }
            return bmp;

        }
        private Bitmap Text2(Bitmap img, string str)
        {
            Bitmap bmp = new Bitmap(img.Width, img.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            using (Graphics g = Graphics.FromImage(bmp))
            {
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                g.DrawImage(img, 0, 0, bmp.Width, bmp.Height);
                g.DrawString(str, new Font("Verdana", 50), new SolidBrush(Color.White), new PointF(0, 0));
            }
            return bmp;

        }

        private Bitmap Text3(Bitmap img, string str)
        {
            Bitmap bmp = new Bitmap(img.Width, img.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            using (Graphics g = Graphics.FromImage(bmp))
            {
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                g.DrawImage(img, 0, 0, bmp.Width, bmp.Height);
                g.DrawString(str, new Font("Verdana", 10), new SolidBrush(Color.Black), new PointF(0, 0));
            }
            return bmp;

        }
        private void ImageProcess()
        {
            // 一、
            // 1. 读取原图
            var imgO = CvInvoke.Imread("OCR1.png");
            var imgO1 = imgO.Clone(); //备用
            CvInvoke.Imshow("imgO",imgO);

            // 2. 灰度
            var imgGray = CvInvoke.Imread("OCR1.png", 0);
            CvInvoke.Imshow("imgGray",imgGray);

            // 4. 高斯滤波
            var imgGaussian = new Mat();
            CvInvoke.GaussianBlur(imgGray, imgGaussian, new Size(5, 5), 0);
            CvInvoke.Imshow("imgGaussian", imgGaussian);

            // 5. Canny/边缘检测
            var imgCanny = new Mat();
            CvInvoke.Canny(imgGaussian,imgCanny,75,200);
            CvInvoke.Imshow("imgCanny", imgCanny);

            // 6. 轮廓检测
            VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint();
            Mat hierarchy = new Mat();
            CvInvoke.FindContours(imgCanny,contours,hierarchy,RetrType.External,ChainApproxMethod.ChainApproxSimple);
            
            // 7. 绘制轮廓
            var imgContours = imgO1.Clone();
            CvInvoke.DrawContours(imgContours,contours,-1,new MCvScalar(0,0,255),2);
            CvInvoke.Imshow("imgContours", imgContours);

            // 8. 轮廓排序
            // 只有一个轮廓，就不排序了

            // 9. 近似矩形
            var length = CvInvoke.ArcLength(contours[0],true);
            VectorOfPoint rec = new VectorOfPoint();
            CvInvoke.ApproxPolyDP(contours[0],rec,length*0.01,true);
            

            // 10 .
            var w1 = Math.Sqrt(Math.Pow(rec[3].X - rec[0].X, 2) + Math.Pow(rec[3].Y - rec[0].Y, 2));
            var w2 = Math.Sqrt(Math.Pow(rec[2].X - rec[1].X, 2) + Math.Pow(rec[2].Y - rec[1].Y, 2));
            var maxWidth = Math.Max(w1,w2);

            var h1 = Math.Sqrt(Math.Pow(rec[1].X - rec[0].X, 2) + Math.Pow(rec[1].Y - rec[0].Y, 2));
            var h2 = Math.Sqrt(Math.Pow(rec[2].X - rec[3].X, 2) + Math.Pow(rec[2].Y - rec[3].Y, 2));
            var maxHeight = Math.Max(h1, h2);

            PointF[] dst = new PointF[4] { new Point(0, 0), new Point((int)maxWidth - 1, 0), new Point((int)maxWidth - 1, (int)maxHeight - 1), new Point(0, (int)maxHeight - 1) };
            PointF[] src = new PointF[4] { rec[0], rec[3], rec[2], rec[1]};

            var transform = CvInvoke.GetPerspectiveTransform(src,dst);
            var imgTransform = new Mat();
            CvInvoke.WarpPerspective(imgO,imgTransform,transform,new Size((int)maxWidth,(int)maxHeight));
            CvInvoke.Resize(imgTransform, imgTransform, imgO.Size);
            CvInvoke.Imshow("imgTransform", imgTransform);

            //CvInvoke.CvtColor(imgTransform,imgTransform,ColorConversion.Bgr2Gray);
            // Mat element = CvInvoke.GetStructuringElement(ElementShape.Rectangle, new Size(3, 3), new Point(-1, -1));
            //CvInvoke.Sobel(imgTransform.Clone(), imgTransform, DepthType.Cv64F, 1, 0, 3);
            //CvInvoke.ConvertScaleAbs(imgTransform, imgTransform, 1.0, 0);
            //CvInvoke.MorphologyEx(imgTransform.Clone(), imgTransform, MorphOp.Close, element, new Point(-1, -1), 1, BorderType.Default, new MCvScalar());
           // CvInvoke.Threshold(imgTransform,imgTransform,135,180,ThresholdType.Binary);
            
           var ocr =  new Tesseract.TesseractEngine("tessdata", "eng", EngineMode.Default);
            
            var text =ocr.Process(imgTransform.Bitmap).GetText();
            var t = text.Trim().Replace("  ","").Split('\n');

            int i = 0;
            int font = 3,thick =5,h=70;
            foreach (var s in t) {
                if (!string.IsNullOrEmpty(s) && s != " " && s!="?" && s != "??" && s != "???") {
                    if (i >1) {
                        font = 1;
                        thick = 1;
                        h = 30;
                    }
                    CvInvoke.PutText(imgTransform, s, new Point(200, 100+i*h), FontFace.HersheyTriplex, font, new MCvScalar(0, 255, 0), thick);
                    i++;
                }
            }
           
            CvInvoke.Imshow("imgTransform2", imgTransform);
            
            CvInvoke.WaitKey(0);
        }
    }
}
