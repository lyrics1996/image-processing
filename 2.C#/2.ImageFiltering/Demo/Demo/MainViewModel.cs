﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace Demo
{
    public class MainViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private WriteableBitmap _previewImage1;

        public WriteableBitmap PreviewImage1
        {
            get { return _previewImage1; }
            set { _previewImage1 = value; PropertyChanged?.Invoke(this,new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage2;

        public WriteableBitmap PreviewImage2
        {
            get { return _previewImage2; }
            set { _previewImage2 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage3;

        public WriteableBitmap PreviewImage3
        {
            get { return _previewImage3; }
            set { _previewImage3 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage4;

        public WriteableBitmap PreviewImage4
        {
            get { return _previewImage4; }
            set { _previewImage4 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }


        private WriteableBitmap _previewImage5;

        public WriteableBitmap PreviewImage5
        {
            get { return _previewImage5; }
            set { _previewImage5 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage6;

        public WriteableBitmap PreviewImage6
        {
            get { return _previewImage6; }
            set { _previewImage6 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        public MainViewModel() {
            ImageProcess();
        }

        private BitmapImage Bitmap2BitmapImage(Bitmap bitmap ) {
            using (MemoryStream ms = new MemoryStream()) {
                bitmap.Save(ms,ImageFormat.Bmp);
                BitmapImage bi = new BitmapImage();
                bi.BeginInit();
                bi.StreamSource = ms;
                bi.CacheOption = BitmapCacheOption.OnLoad;
                bi.EndInit();
                bi.Freeze();
                return bi;
            }
        }
        private void ImageProcess()
        {
            // 1. 加载原图
            Mat image1 = CvInvoke.Imread("test.png");
            PreviewImage1 = new WriteableBitmap(Bitmap2BitmapImage(image1.Bitmap));
            // 2. 中值滤波
            Mat image2 = new Mat();
            CvInvoke.MedianBlur(image1,image2,1);
            PreviewImage2 = new WriteableBitmap(Bitmap2BitmapImage(image2.Bitmap));
            // 3. 均值滤波
            Mat image3 = new Mat();
            CvInvoke.Blur(image1, image3, new Size(10,10),new Point(-1,-1));
            PreviewImage3 = new WriteableBitmap(Bitmap2BitmapImage(image3.Bitmap));
            // 4. 高斯滤波
            Mat image4 = new Mat();
            CvInvoke.GaussianBlur(image1, image4, new Size(5, 5), 4);
            PreviewImage4 = new WriteableBitmap(Bitmap2BitmapImage(image4.Bitmap));
            // 5. 双边滤波
            Mat image5 = new Mat();
            CvInvoke.BilateralFilter(image1, image5, 10,30,15);
            PreviewImage5 = new WriteableBitmap(Bitmap2BitmapImage(image5.Bitmap));
            // 6. 方框滤波
            Mat image6 = new Mat();
            CvInvoke.BoxFilter(image1, image6, DepthType.Cv8U,new Size(5,5),new Point(-1,-1));
            PreviewImage6 = new WriteableBitmap(Bitmap2BitmapImage(image6.Bitmap));       
        }
    }
}
