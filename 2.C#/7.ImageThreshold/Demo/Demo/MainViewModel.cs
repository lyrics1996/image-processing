﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Media.Imaging;

namespace Demo
{
    public class MainViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private WriteableBitmap _previewImage1;

        public WriteableBitmap PreviewImage1
        {
            get { return _previewImage1; }
            set { _previewImage1 = value; PropertyChanged?.Invoke(this,new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage2;

        public WriteableBitmap PreviewImage2
        {
            get { return _previewImage2; }
            set { _previewImage2 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage3;

        public WriteableBitmap PreviewImage3
        {
            get { return _previewImage3; }
            set { _previewImage3 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage4;

        public WriteableBitmap PreviewImage4
        {
            get { return _previewImage4; }
            set { _previewImage4 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }


        private WriteableBitmap _previewImage5;

        public WriteableBitmap PreviewImage5
        {
            get { return _previewImage5; }
            set { _previewImage5 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage6;

        public WriteableBitmap PreviewImage6
        {
            get { return _previewImage6; }
            set { _previewImage6 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage7;

        public WriteableBitmap PreviewImage7
        {
            get { return _previewImage7; }
            set { _previewImage7 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }
        private WriteableBitmap _previewImage8;

        public WriteableBitmap PreviewImage8
        {
            get { return _previewImage8; }
            set { _previewImage8 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }
        private WriteableBitmap _previewImage9;

        public WriteableBitmap PreviewImage9
        {
            get { return _previewImage9; }
            set { _previewImage9 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }
        public MainViewModel() {
            ImageProcess();
        }

        private BitmapImage Bitmap2BitmapImage(Bitmap bitmap ) {
            using (MemoryStream ms = new MemoryStream()) {
                bitmap.Save(ms,ImageFormat.Bmp);
                BitmapImage bi = new BitmapImage();
                bi.BeginInit();
                bi.StreamSource = ms;
                bi.CacheOption = BitmapCacheOption.OnLoad;
                bi.EndInit();
                bi.Freeze();
                return bi;
            }
        }
        private void ImageProcess()
        {
            // 1. 加载原图1
            var image1 = new Image<Bgr, byte>("bird1.png");
            PreviewImage1 = new WriteableBitmap(Bitmap2BitmapImage(image1.Bitmap));

            // 2. 加载灰度图
            var image2 = new Image<Gray, byte>("bird1.png");
            PreviewImage2 = new WriteableBitmap(Bitmap2BitmapImage(image2.Bitmap));

            // 3 
            var image3 = new Image<Gray, byte>(image1.Size);
            CvInvoke.Threshold(image2,image3,100,255,ThresholdType.Binary);
            PreviewImage3 = new WriteableBitmap(Bitmap2BitmapImage(image3.Bitmap));

            CvInvoke.Threshold(image2, image3, 100, 255, ThresholdType.BinaryInv);
            PreviewImage4 = new WriteableBitmap(Bitmap2BitmapImage(image3.Bitmap));

            CvInvoke.Threshold(image2, image3, 100, 255, ThresholdType.Trunc);
            PreviewImage5 = new WriteableBitmap(Bitmap2BitmapImage(image3.Bitmap));

            CvInvoke.Threshold(image2, image3, 100, 255, ThresholdType.ToZero);
            PreviewImage6 = new WriteableBitmap(Bitmap2BitmapImage(image3.Bitmap));

            CvInvoke.Threshold(image2, image3, 100, 255, ThresholdType.ToZeroInv);
            PreviewImage7 = new WriteableBitmap(Bitmap2BitmapImage(image3.Bitmap));

            CvInvoke.Threshold(image2, image3, 100, 255, ThresholdType.Mask);
            PreviewImage8 = new WriteableBitmap(Bitmap2BitmapImage(image3.Bitmap));

            CvInvoke.Threshold(image2, image3, 0, 255, ThresholdType.Otsu);
            PreviewImage9 = new WriteableBitmap(Bitmap2BitmapImage(image3.Bitmap));

            //var image3 = new Image<Gray, byte>(image1.Size);
            //CvInvoke.AdaptiveThreshold(image2, image3, 255, AdaptiveThresholdType.MeanC, ThresholdType.Binary, 11, 5);
            //PreviewImage4 = new WriteableBitmap(Bitmap2BitmapImage(image3.Bitmap));

            //CvInvoke.AdaptiveThreshold(image2, image3, 255, AdaptiveThresholdType.MeanC, ThresholdType.BinaryInv, 11, 5);
            //PreviewImage5 = new WriteableBitmap(Bitmap2BitmapImage(image3.Bitmap));

            //CvInvoke.AdaptiveThreshold(image2, image3, 255, AdaptiveThresholdType.GaussianC, ThresholdType.Binary, 11, 5);
            //PreviewImage7 = new WriteableBitmap(Bitmap2BitmapImage(image3.Bitmap));

            //CvInvoke.AdaptiveThreshold(image2, image3, 255, AdaptiveThresholdType.GaussianC, ThresholdType.BinaryInv, 11, 5);
            //PreviewImage8 = new WriteableBitmap(Bitmap2BitmapImage(image3.Bitmap));
        }
    }
}
