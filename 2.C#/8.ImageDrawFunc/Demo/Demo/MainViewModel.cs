﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Media.Imaging;

namespace Demo
{
    public class MainViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private WriteableBitmap _previewImage1;

        public WriteableBitmap PreviewImage1
        {
            get { return _previewImage1; }
            set { _previewImage1 = value; PropertyChanged?.Invoke(this,new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage2;

        public WriteableBitmap PreviewImage2
        {
            get { return _previewImage2; }
            set { _previewImage2 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage3;

        public WriteableBitmap PreviewImage3
        {
            get { return _previewImage3; }
            set { _previewImage3 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage4;

        public WriteableBitmap PreviewImage4
        {
            get { return _previewImage4; }
            set { _previewImage4 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }


        private WriteableBitmap _previewImage5;

        public WriteableBitmap PreviewImage5
        {
            get { return _previewImage5; }
            set { _previewImage5 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage6;

        public WriteableBitmap PreviewImage6
        {
            get { return _previewImage6; }
            set { _previewImage6 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }

        private WriteableBitmap _previewImage7;

        public WriteableBitmap PreviewImage7
        {
            get { return _previewImage7; }
            set { _previewImage7 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }
        private WriteableBitmap _previewImage8;

        public WriteableBitmap PreviewImage8
        {
            get { return _previewImage8; }
            set { _previewImage8 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }
        private WriteableBitmap _previewImage9;

        public WriteableBitmap PreviewImage9
        {
            get { return _previewImage9; }
            set { _previewImage9 = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("")); }
        }
        public MainViewModel() {
            ImageProcess();
        }

        private BitmapImage Bitmap2BitmapImage(Bitmap bitmap ) {
            using (MemoryStream ms = new MemoryStream()) {
                bitmap.Save(ms,ImageFormat.Bmp);
                BitmapImage bi = new BitmapImage();
                bi.BeginInit();
                bi.StreamSource = ms;
                bi.CacheOption = BitmapCacheOption.OnLoad;
                bi.EndInit();
                bi.Freeze();
                return bi;
            }
        }
        private void ImageProcess()
        {
            // 1. 加载原图1
            var image1 = new Image<Bgr, byte>("bird1.png");
            var image0 = image1.Clone();
            CvInvoke.PutText(image1,"Original",new Point(200,400),FontFace.HersheyTriplex, 3, new MCvScalar(0, 0, 255), 5);
            PreviewImage1 = new WriteableBitmap(Bitmap2BitmapImage(image1.Bitmap));
            

            // 2. 绘制直线
            var image2 = new Image<Bgr, byte>(image0.Data);
            CvInvoke.Line(image2, new Point(50, 50), new Point(450, 450), new MCvScalar(0, 255, 255), 5, LineType.FourConnected);
            PreviewImage2 = new WriteableBitmap(Bitmap2BitmapImage(image2.Bitmap));

            // 2. 圆
            var image3 = new Image<Bgr, byte>(image0.Data);
            CvInvoke.Circle(image3, new Point(250, 250), 150, new MCvScalar(255, 0, 0), 2, LineType.FourConnected);
            PreviewImage3 = new WriteableBitmap(Bitmap2BitmapImage(image3.Bitmap));

            // 4. 实心圆
            var image4 = new Image<Bgr, byte>(image0.Data);
            CvInvoke.Circle(image4, new Point(250, 250), 50, new MCvScalar(255, 0, 0), -1, LineType.FourConnected);
            PreviewImage4 = new WriteableBitmap(Bitmap2BitmapImage(image4.Bitmap));

            // 5. 矩形
            var image5 = new Image<Bgr, byte>(image0.Data);
            CvInvoke.Rectangle(image5, new Rectangle(100, 100, 300, 200), new MCvScalar(225, 225, 0), 5, LineType.FourConnected);
            PreviewImage5 = new WriteableBitmap(Bitmap2BitmapImage(image5.Bitmap));

            // 6. 实心矩形
            var image6 = new Image<Bgr, byte>(image0.Data);
            CvInvoke.Rectangle(image6, new Rectangle(100, 100, 300, 200), new MCvScalar(225, 225, 0), -1, LineType.FourConnected);
            PreviewImage6 = new WriteableBitmap(Bitmap2BitmapImage(image6.Bitmap));

            // 7. 椭圆
            var image7 = new Image<Bgr, byte>(image0.Data);
            CvInvoke.Ellipse(image7, new RotatedRect(new PointF(250, 250), new SizeF(100, 200), 0), new MCvScalar(200, 0, 0), 5);
            PreviewImage7 = new WriteableBitmap(Bitmap2BitmapImage(image7.Bitmap));

            // 8. 实心椭圆
            var image8 = new Image<Bgr, byte>(image0.Data);
            CvInvoke.Ellipse(image8, new RotatedRect(new PointF(250, 250), new SizeF(100, 200), 0), new MCvScalar(200, 0, 0),-1);
            PreviewImage8 = new WriteableBitmap(Bitmap2BitmapImage(image8.Bitmap));

            // 9. 矩形加椭圆
            var image9 = new Image<Bgr, byte>(image0.Data);
            CvInvoke.Ellipse(image9, new Point(250, 250), new Size(150, 100), 45, 0, 270, new MCvScalar(0, 0, 255), -2);
            PreviewImage9 = new WriteableBitmap(Bitmap2BitmapImage(image9.Bitmap));
        }
    }
}
